# L2, L3 labs.
В данном разделе представлены лабораторные работы по L2 (канальному) и L3 (сетевому) уровням модели сети OSI.
Список лабораторных работ:
- Лабораторная работа 0. Установка и знакомство с ПО (Необязательная, для ознакомления с инструментами).
    - Краткий обзор ПО.
    - Пример установки pnetlab (EVE-NG)
- Лабораторная работа 1.
    Изучите и выполните следующие атаки. Настройте защиту от этих атак и проверьте, что атаки непроходят после этого:
    - ARP-spoofing attack.
    - VLAN-hopping attack.
    - CAM Overflow attack.
    - DHCP Starvation attack.
- Лабораторная работа 2
    Изучите и выполните следующие атаки. Настройте защиту от этих атак и проверьте, что атаки непроходят после этого:
    - STP L2 attack.
    - DDOS attack.
    - ICMP redirect man-in-the-middle attack.
- Лабораторная работа 3
    Изучите и выполните следующие атаки. Настройте защиту от этих атак и проверьте, что атаки непроходят после этого:
    - OSPF route spoofing
    - BGP route spoofing

Рекомендации по выполнению лабораторных работ представлены в директория [lab0](/lab0/), [lab1](/lab1/), [lab2](/lab2/), [lab3](/lab3/) и в файле [labs.pdf](/labs.pdf)

Для выполнения лабораторных работ предлагаются следующие программные комплексы:
    - GNS3
    - EVE-NG
    - pnetlab