import socket
import threading
import ssl

class MultiThreadedHTTPServer:
    def __init__(self, host, port, certificate=None, key=None):
        self.host = host
        self.port = port
        self.certificate = certificate
        self.key = key
        self.server_socket = None

    def start(self):
        # Создаем сокет сервера
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        
        # Если заданы сертификат и ключ, использовать SSL/TLS
        if self.certificate and self.key:
            context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
            context.load_cert_chain(certfile=self.certificate, keyfile=self.key)
            self.server_socket = context.wrap_socket(self.server_socket, server_side=True)
        
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(5)
        print(f"Сервер запущен на {self.host}:{self.port}")

        while True:
            # Принимаем клиентское соединение
            client_socket, client_address = self.server_socket.accept()
            print(f"Установлено соединение с клиентом {client_address[0]}:{client_address[1]}")

            # Создаем отдельный поток для обработки клиента
            client_thread = threading.Thread(target=self.handle_client, args=(client_socket,))
            client_thread.start()

    def handle_client(self, client_socket):
        # Читаем данные от клиента
        request = client_socket.recv(1024).decode()
        
        # Выводим полученный запрос
        print(f"Получен запрос: {request}")

        # Отправляем ответ клиенту
        response = "HTTP/1.1 200 OKnContent-Type: text/htmlnnПривет, клиент!"
        client_socket.send(response.encode())

        # Закрываем соединение с клиентом
        client_socket.close()

    def stop(self):
        if self.server_socket:
            self.server_socket.close()
            print("Сервер остановлен")

# Пример использования
host = "127.0.0.1"
port = 8080
certificate_file = None  # Путь к файлу сертификата, если используется SSL/TLS
key_file = None  # Путь к файлу ключа, если используется SSL/TLS

# Создаем экземпляр сервера
server = MultiThreadedHTTPServer(host, port, certificate_file, key_file)

# Запускаем сервер
server.start()