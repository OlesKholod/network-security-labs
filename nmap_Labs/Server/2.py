import socket
import threading
import ssl
import time
import signal
from tqdm import tqdm

DEBUG = True

def signal_handler(signal, frame):
    if DEBUG:
        print("\nYou've pressed comb `Ctrl+C!`")
    print("\nServer will be downed")
    for i in tqdm(range(30)):
        time.sleep(0.05)
    print("Server is down.")
    exit(0)

class MultiThreadedChatServer:
    def __init__(self, host, port, certificate=None, key=None):
        self.host = host
        self.port = port
        self.certificate = certificate
        self.key = key
        self.server_socket = None
        self.clients = {}  # Словарь для хранения соединения и имени клиентов

    def start(self):
        # Создаем сокет сервера
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        
        # Если заданы сертификат и ключ, использовать SSL/TLS
        if self.certificate and self.key:
            context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
            context.load_cert_chain(certfile=self.certificate, keyfile=self.key)
            self.server_socket = context.wrap_socket(self.server_socket, server_side=True)
        
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen(5)
        print(f"Сервер запущен на {self.host}:{self.port}")

        while True:
            # Принимаем клиентское соединение
            client_socket, client_address = self.server_socket.accept()
            print(f"Установлено соединение с клиентом {client_address[0]}:{client_address[1]}")

            # Запрашиваем имя клиента
            client_name = self.request_client_name(client_socket)
            self.clients[client_socket] = client_name

            # Создаем отдельный поток для обработки клиента
            client_thread = threading.Thread(target=self.handle_client, args=(client_socket,))
            client_thread.start()

    def request_client_name(self, client_socket):
        # Запрос имени клиента
        client_socket.send("Введите имя: ".encode())
        client_name = client_socket.recv(1024).decode().strip()
        return client_name

    def handle_client(self, client_socket):
        # Приветствие клиента
        client_socket.send("Добро пожаловать в чат!\n".encode())

        while True:
            try:
                # Читаем сообщение от клиента
                message = client_socket.recv(1024).decode().strip()
                if message:
                    # Отправляем сообщение всем клиентам
                    self.send_message_to_all(f"{self.clients[client_socket]}: {message}")
            except (ConnectionResetError, ConnectionAbortedError):
                # Если клиент отключился, удаляем его из списка клиентов
                print(f"Клиент {self.clients[client_socket]} отключен")
                del self.clients[client_socket]
                break

    def send_message_to_all(self, message):
        # Отправляем сообщение всем клиентам
        for client_sock in self.clients:
            client_sock.send(f"{message}\n".encode())

    def stop(self):
        if self.server_socket:
            self.server_socket.close()
            print("Сервер остановлен")


signal.signal(signal.SIGINT, signal_handler)
# Пример использования
host = "127.0.0.1"
port = 8080
certificate_file = None  # Путь к файлу сертификата, если используется SSL/TLS
key_file = None  # Путь к файлу ключа, если используется SSL/TLS

# Создаем экземпляр сервера
server = MultiThreadedChatServer(host, port, certificate_file, key_file)

# Запускаем сервер
server.start()