from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import ThreadedFTPServer

def start_ftp_server(host, port, username, password, root_directory):
    authorizer = DummyAuthorizer()
    authorizer.add_user(username, password, root_directory, perm='elradfmwM')

    handler = FTPHandler
    handler.authorizer = authorizer

    address = (host, port)
    server = ThreadedFTPServer(address, handler)
    print(f"FTP server started on {host}:{port}")

    server.serve_forever()

# Задаем параметры хоста и порта сервера FTP
host = "127.0.0.1"
port = 21

# Задаем имя пользователя и пароль для подключения к серверу FTP
username = "your_username"
password = "your_password"

# Задаем корневую директорию сервера FTP
root_directory = "/path/to/root/directory"

# Запускаем многопоточный FTP-сервер
start_ftp_server(host, port, username, password, root_directory)