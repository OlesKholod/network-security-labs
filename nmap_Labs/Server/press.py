import socket
import threading

def connect_to_server(host, port):
    try:
        # Создаем сокет клиента
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((host, port))
        print(f"Подключено к серверу {host}:{port}")

        # Отправляем данные на сервер
        client_socket.send("Hello, server!".encode())

        # Получаем ответ от сервера
        response = client_socket.recv(1024).decode()
        print(f"Ответ от сервера: {response}")

        # Закрываем клиентский сокет
        client_socket.close()
        print(f"Отключено от сервера {host}:{port}")
    except ConnectionRefusedError:
        print(f"Не удалось подключиться к серверу {host}:{port}")

# Задаем параметры сервера
host = "127.0.0.1"
port = 8080

# Задаем количество подключений
num_connections = 500

# Подключаемся к серверу в несколько потоков
threads = []
for _ in range(num_connections):
    t = threading.Thread(target=connect_to_server, args=(host, port))
    t.start()
    threads.append(t)

# Ждем завершения всех потоков
for t in threads:
    t.join()