import os
import threading
import paramiko

class SSHServer(paramiko.ServerInterface):
    def __init__(self):
        self.event = threading.Event()

    def check_auth_password(self, username, password):
        # Здесь можно проводить аутентификацию пользователей
        # Возврат True разрешает аутентификацию, False - запрещает
        return True

    def check_channel_request(self, kind, channel_id):
        if kind == 'session':
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED

    def check_auth_publickey(self, username, key):
        # Метод для аутентификации с использованием публичного ключа SSH
        # Возврат True разрешает аутентификацию, False - запрещает
        return False

def handle_connection(client):
    transport = paramiko.Transport(client)
    transport.add_server_key(paramiko.RSAKey.generate(2048))
    server = SSHServer()
    transport.start_server(server=server)

    channel = transport.accept(20)
    if channel is None:
        transport.close()
        return

    while transport.is_active() and not channel.closed:
        command = channel.recv(1024).decode().strip()
        if command.startswith("scp "):
            # Обработка команды SCP для передачи файла
            parts = command.split()
            if len(parts) != 3:
                channel.send("Invalid SCP command")
                continue

            _, source, destination = parts
            try:
                channel.send("Ok")
                transport.subsystem("scp -t " + destination)
                scp_recv(channel, source)
                channel.send("x00")
            except Exception as e:
                channel.send("Error: " + str(e))
        elif command == 'exit':
            break
        else:
            channel.send("Unknown command")
    
    channel.close()
    transport.close()

def scp_recv(channel, filename):
    filesize = os.stat(filename).st_size
    command = f"C0644 {filesize} {filename}n"
    channel.send(command.encode())

    with open(filename, 'rb') as f:
        data = f.read(32768)
        while data:
            channel.send(data)
            data = f.read(32768)
    
    channel.send('x00')

def start_ssh_server(host, port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(5)
    print(f"SSH server started on {host}:{port}")

    while True:
        client, addr = server_socket.accept()
        print(f"Incoming connection from {addr[0]}:{addr[1]}")

        # Обрабатываем каждое соединение в отдельном потоке
        t = threading.Thread(target=handle_connection, args=(client,))
        t.start()

# Задаем параметры хоста и порта сервера SSH
host = "0.0.0.0"
port = 22

# Запускаем многопоточный SSH-сервер
start_ssh_server(host, port)