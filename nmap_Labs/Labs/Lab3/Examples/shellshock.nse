local http = require("http")
local shortport = require("shortport")
local nmap = require("nmap")
local stdnse = require("stdnse")

description = [[
Checks for the Shellshock vulnerability (CVE-2014-6271 and CVE-2014-7169) on HTTP servers.
]]

---
-- @output
-- PORT     STATE SERVICE
-- 80/tcp   open  http
-- |_shellshock: Shellshock vulnerability (CVE-2014-6271 and CVE-2014-7169) present.
-- MAC Address: XX:XX:XX:XX:XX:XX (Manufacturer)
--
-- @args shellshock.uri The URI to use for testing Shellshock vulnerability
-- (default: /cgi-bin/showenv)
--
-- @xmloutput

author = "Nmap Project"
license = "Same as Nmap--See https://nmap.org/book/man-legal.html"
categories = {"exploit", "intrusive"}

portrule = shortport.http

action = function(host, port)
  local script_args = {
    uri = stdnse.get_script_args("shellshock.uri") or "/cgi-bin/showenv"
  }

  local result, response = http.get(host, port, script_args.uri)

  if result == http.ALIVE then
    if response.body and (string.find(response.body, "bash.-bad") or
                          string.find(response.body, "bash.-overflow")) then
      nmap.set_port_state(port, "open")
      stdnse.debug("Shellshock vulnerability (CVE-2014-6271 and CVE-2014-7169) present.")
      stdnse.debug("BODY: n" .. response.body)
      stdnse.set_script_output(nmap.get_port_state().. "/tcp  open  httpn|_shellshock: Shellshock vulnerability (CVE-2014-6271 and CVE-2014-7169) present.")
      return
    end
  end

  nmap.set_port_state(port, "closed")
end
