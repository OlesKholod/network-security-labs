local dns = require("dns")

action = function(host)
  -- Указываем домен, для которого необходимо найти DNS-записи
  local domain = "<домен>"
  
  -- Запрашиваем DNS-записи для указанного домена
  local res, err = dns.query(domain)
  
  if res then
    local records = res:copy_answers()
    
    -- Выводим информацию о каждой найденной записи
    for _, record in ipairs(records) do
      local rrname = record:name()
      local rrtype = record:type()
      local rdata = record:rdata()
      
      -- Выводим информацию о записи
      local info = string.format("Запись: %s, Тип: %s, Значение: %s", rrname, rrtype, rdata)
      
      print(info)
    end
  else
    -- Выводим информацию об ошибке
    print("Не удалось получить DNS-записи:", err)
  end
end
