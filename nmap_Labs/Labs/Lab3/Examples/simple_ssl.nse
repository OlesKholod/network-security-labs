local openssl = require("openssl")
local shortport = require("shortport")

portrule = shortport.port_or_service(443, "https")

action = function(host, port)
  local ctx = openssl.ctx_new("TLSv1_2", "ALL:!EXPORT:!LOW:!MD5:!DES:!RC4:!SSLv2:!SSLv3")
  local ssl = openssl.ssl_new(ctx)
  local sock = nmap.new_ssl_socket(ssl)
  local result, response = sock:connect(host.ip, port.number, {"tcp"})
  
  if result then
    -- Выводим информацию о версии протокола
    nmap.set_ssl_version(ssl:version())
  
    -- Проверяем на уязвимости в SSL/TLS
    local vulnerabilities = {}
    
    -- Проверка на использование устаревших протоколов
    if ssl:version() == "TLSv1" or ssl:version() == "SSLv3" then
      table.insert(vulnerabilities, "Используется устаревший протокол: " .. ssl:version())
    end
    
    -- Проверка на использование слабых шифров
    local cipher = openssl.ssl_get_cipher(ssl)
    if cipher:bits() < 128 or cipher:algorithm() == "RC4" then
      table.insert(vulnerabilities, "Используется слабый шифр: " .. cipher:info())
    end
    
    -- Выводим информацию об уязвимостях
    if #vulnerabilities > 0 then
      local status = {}
      table.insert(status, "Уязвимости SSL/TLS обнаружены на хосте " .. host.ip .. ":" .. port.number)
      for _, v in ipairs(vulnerabilities) do
        table.insert(status, " - " .. v)
      end
      nmap.set_script_status(table.concat(status, "n"))
    end
  else
    -- Выводим информацию об ошибке подключения
    nmap.set_script_status("Не удалось установить защищенное соединение: " .. response)
  end
  
  -- Закрываем соединение
  sock:close()
end
