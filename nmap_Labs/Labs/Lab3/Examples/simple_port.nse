local shortport = require "shortport"

portrule = shortport.port_or_service({80, 443, 8080}, {"http", "https"})

action = function(host, port)
  local state = "closed"
  local sock = nmap.new_socket()
  local result = sock:connect(host.ip, port.number, {"tcp"})
  if result == "open" then
    state = "open"
  elseif result == "filtered" then
    state = "filtered"
  end
  sock:close()

  if state == "open" then
    nmap.output_table(port)
  end
end