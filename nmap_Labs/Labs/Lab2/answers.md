1. Проведите сканирование с определением версий служб с интенсивностью и отслеживанием:
   - Команда: nmap -sV --version-intensity 5 --version-trace target_ip
   - Описание: Выполнить сканирование целевого IP-адреса для определения версий служб с интенсивностью 5 и отслеживанием процесса сканирования.

2. Проведите сканирование с сканированием всех портов с обнаружением ОС и угадыванием результатов:
   - Команда: nmap -p- -O --osscan-guess target_ip
   - Описание: Провести сканирование всех портов на целевой машине для определения доступности их идентификации, определения операционной системы и угадывания результатов на основе сравнения характеристик ОС.

3. Проведите сканирование с регулированием времени ожидания ответа и размера групп для параллельного сканирования:
   - Команда: nmap --min-rtt-timeout 500 --max-rtt-timeout 2000 --initial-rtt-timeout 1000 --min-hostgroup 5 --max-hostgroup 10 target_ip
   - Описание: Настроить время ожидания ответа на запросы от 500 до 2000 миллисекунд и время инициализации запроса на 1000 миллисекунд, а также размер группы для параллельного сканирования от 5 до 10 хостов.

4. Проведите сканирование с маскировкой сканирования и использованием фиктивных хостов:
   - Команда: nmap -D decoy1,decoy2,target_ip
   - Описание: Выполнить сканирование, используя фиктивные хосты decoy1 и decoy2 для маскировки сканирования и усложнения обнаружения.

5. Проведите сканирование с изменением исходного адреса и определением своего номера порта:
   - Команда: nmap -S spoofed_ip -p custom_port target_ip
   - Описание: Выполнить сканирование с измененным исходным IP-адресом на spoofed_ip и указанием своего номера порта custom_port.

6. Проведите сканирование с фрагментацией пакетов и добавлением произвольных данных:
   - Команда: nmap -f --data-length 100 target_ip
   - Описание: Сканирование с использованием фрагментации пакетов и добавлением произвольных данных длиной 100 байт для обхода сетевых ограничений и систем обнаружения вторжений.

7. Проведите сканирование с маскировкой МАС адреса и отправкой пакетов с фиктивными контрольными суммами:
   - Команда: nmap --spoof-mac fake_mac --badsum target_ip
   - Описание: Сканирование с маскировкой МАС адреса на fake_mac и отправкой пакетов с фиктивными TCP/UDP контрольными суммами для обхода брандмауэров и систем обнаружения вторжений.

8. Проведите сканирование с комбинированным сканированием с опцией определения ОС и регулированием интенсивности:
   - Команда: nmap -p 1-100 -O --version-intensity 3 target_ip
   - Описание: Выполнить сканирование портов с 1 по 100 для определения доступности служб и операционной системы, используя интенсивность 3.

9. Проведите сканирование со сканированием с использованием скриптов NSE:
   - Команда: nmap -sC target_ip
   - Описание: Выполнить сканирование с использованием скриптов Nmap Scripting Engine (NSE), расширяющих возможности сканирования, обнаружения уязвимостей и других операций.

10. Проведите сканирование с проведением сканирования в фоновом режиме:
   - Команда: nmap -p 1-100 -O -oN output.txt -bg target_ip
   - Описание: Запустить сканирование портов с 1 по 100 для определения служб и операционной системы в фоновом режиме, сохраняя результаты в файл output.txt.