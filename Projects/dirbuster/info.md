```
import requests

def dirbust(target_url, wordlist_file):
    try:
        # Открытие файла со словарем
        with open(wordlist_file) as f:
            wordlist = f.read().splitlines()
        
        # Перебор и проверка каждого имени файла
        for word in wordlist:
            url = target_url + "/" + word
            response = requests.head(url)
            
            # Проверка HTTP-кода ответа
            if response.status_code == 200:
                print(f"Found: {url}")
    
    except IOError:
        print("Failed to open the wordlist file")

# Пример вызова функции dirbust
target_url = "http://example.com"  # Укажите целевой URL
wordlist_file = "wordlist.txt"  # Укажите путь к файлу со словарем

dirbust(target_url, wordlist_file)

```