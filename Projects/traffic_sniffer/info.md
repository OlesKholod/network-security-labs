```python
import pcapy

def packet_handler(header, data):
    print(data)

# Открываем сетевой интерфейс для захвата пакетов
interface = 'eth0'  # Замените 'eth0' на имя вашего сетевого интерфейса
capture = pcapy.open_live(interface, 65536, True, 0)

# Запускаем бесконечный цикл для захвата пакетов
capture.loop(0, packet_handler)
```

```python
from scapy.all import *

def packet_callback(packet):
    if packet[TCP].payload:
        # Изменяем содержимое пакета
        packet[TCP].payload = "Здравствуй, мир!"

        # Удаляем контрольную сумму и длину пакета со старыми значениями, чтобы Scapy пересчитал их
        del packet[TCP].chksum
        del packet[IP].len

        # Отправляем измененный пакет
        send(packet)

# Слушаем трафик на интерфейсе eth0
sniff(iface="eth0", filter="tcp", prn=packet_callback)
```

```python
import socket
import struct

# Создаем сокет для прослушивания пакетов
raw_socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))

# Бесконечный цикл для перехвата пакетов
while True:
    packet_data, address = raw_socket.recvfrom(65535)

    # Разбираем заголовок Ethernet
    eth_header = packet_data[:14]
    eth_header_unpack = struct.unpack("!6s6s2s", eth_header)
    source_mac = eth_header_unpack[0]
    dest_mac = eth_header_unpack[1]
    eth_type = eth_header_unpack[2]

    # Работа с пакетом
    # здесь вы можете анализировать или изменять его содержимое

    # Перехваченные пакеты выводятся на экран
    print(f"Перехвачен пакет от {source_mac} до {dest_mac}, тип: {eth_type}")

```