
```python
from scapy.all import *

def packet_analyzer(packet):
    # Анализируем только пакеты типа TCP/IP
    if IP in packet and TCP in packet:
        src_ip = packet[IP].src
        dst_ip = packet[IP].dst
        src_port = packet[TCP].sport
        dst_port = packet[TCP].dport
        
        # Выводим информацию о каждом пакете
        print(f"Source IP: {src_ip}, Source Port: {src_port}")
        print(f"Destination IP: {dst_ip}, Destination Port: {dst_port}")
        print("-" * 30)

# Захватываем и анализируем пакеты с помощью фильтра
sniff(filter="tcp", prn=packet_analyzer)
```

```python
from scapy.all import *

def packet_analyzer(packet):
    # Анализируем только пакеты типа TCP/IP
    if IP in packet and TCP in packet:
        # Выводим содержимое пакета
        print(packet.show())

# Захватываем и анализируем пакеты с помощью фильтра
sniff(filter="tcp", prn=packet_analyzer)

```