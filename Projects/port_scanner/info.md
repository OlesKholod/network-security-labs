
```
import socket

def scan_port(target_host, target_port):
    try:
        # Создание сокета и установка тайм-аута в 1 секунду
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)
        
        # Попытка подключиться к указанному хосту и порту
        result = sock.connect_ex((target_host, target_port))
        
        # Проверка результата
        if result == 0:
            print(f"Port {target_port} is open")
        else:
            print(f"Port {target_port} is closed")
        
        # Закрытие сокета
        sock.close()

    except socket.error:
        print(f"Failed to connect to {target_host}:{target_port}")

# Пример вызова функции scan_port
target_host = "example.com"
target_ports = [80, 443, 22, 21, 3389]  # Пример списка портов для сканирования

for port in target_ports:
    scan_port(target_host, port)

```


```
import socket

def check_ssh_port(target_host, target_port):
    try:
        # Создание сокета и установка тайм-аута в 1 секунду
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)
        
        # Попытка подключиться к указанному хосту и порту
        result = sock.connect_ex((target_host, target_port))
        
        # Проверка результата
        if result == 0:
            # Если удалось установить соединение, отправляем приветственное сообщение SSH
            sock.send(b"SSH-2.0-OpenSSHrn")
            response = sock.recv(1024)
            
            # Проверка ответа на содержание "OpenSSH", что указывает на SSH-сервер
            if b"OpenSSH" in response:
                print(f"Port {target_port} is an SSH port")
            else:
                print(f"Port {target_port} is open, but not an SSH port")
        else:
            print(f"Port {target_port} is closed")
        
        # Закрытие сокета
        sock.close()
    
    except socket.error:
        print(f"Failed to connect to {target_host}:{target_port}")

# Пример вызова функции check_ssh_port
target_host = "example.com"  # Укажите целевой хост
target_port = 22  # Укажите целевой порт

check_ssh_port(target_host, target_port)
```