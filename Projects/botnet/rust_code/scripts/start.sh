#!/bin/bash

# Обновление пакетного менеджера
sudo apt update

# Установка curl
sudo apt install curl

# Установка Rust через rustup
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Компиляция simple_port_scan.rs
rustc simple_port_scan.rs

# Компиляция services_scan.rs
rustc services_scan.rs