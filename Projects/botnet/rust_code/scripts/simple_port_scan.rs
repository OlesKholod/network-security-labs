use std::env;
use std::fs::File;
use std::io::{self, Write};
use std::net::TcpStream;
use std::str::FromStr;

fn main() {
    // Получение аргументов командной строки
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Usage: ./port_scanner <ip_address>");
        return;
    }

    let ip_address = &args[1];

    // Создание файла для записи результатов
    let file_name = format!("{}_open_ports.txt", ip_address);
    let mut file = match File::create(&file_name) {
        Ok(file) => file,
        Err(err) => {
            eprintln!("Failed to create file {}: {}", file_name, err);
            return;
        }
    };

    // Сканирование портов
    for port in 1..=65535 {
        let address = format!("{}:{}", ip_address, port);
        match TcpStream::connect_timeout(&address.parse().unwrap(), std::time::Duration::from_secs(1)) {
            Ok(_) => {
                // Порт открыт
                println!("Port {} is open", port);
                if let Err(err) = writeln!(file, "Port {} is open", port) {
                    eprintln!("Failed to write to file: {}", err);
                }
            }
            Err(_) => {
                // Порт закрыт
            }
        }
    }
}

