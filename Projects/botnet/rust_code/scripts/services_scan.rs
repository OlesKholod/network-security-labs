use std::process::{Command, exit};
use std::fs::File;
use std::io::Write;

fn main() {
    println!("Start");

    let output = {
        // Используем service, если systemd не обнаружен
        println!("service");
        Command::new("service")
            .arg("--status-all")
            .output()
    };

    let output = match output {
        Ok(output) => output,
        Err(err) => {
            eprintln!("Failed to execute command: {}", err);
            exit(1);
        }
    };

    // Проверяем код возврата команды
    if !output.status.success() {
        eprintln!("Command failed with exit code: {}", output.status);
        if !output.stderr.is_empty() {
            eprintln!("{}", String::from_utf8_lossy(&output.stderr));
        }
        exit(1);
    }

    // Создаем имя файла используя шаблон <ip_address>_services.txt
    let ip_address = "192.168.0.1"; // Замените на соответствующий IP-адрес
    let filename = format!("{}_services.txt", ip_address);

    // Записываем результаты в файл
    match File::create(&filename) {
        Ok(mut file) => {
            if let Err(err) = file.write_all(&output.stdout) {
                eprintln!("Failed to write to file: {}", err);
                exit(1);
            }
            println!("Data saved to file: {}", filename);
        }
        Err(err) => {
            eprintln!("Failed to create file: {}", err);
            exit(1);
        }
    }
}

