use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use std::thread;
use rayon::ThreadPoolBuilder;

const CLIENT_NUM: usize = 100;

fn handle_client(mut stream: TcpStream) {
    let mut buffer = [0; 512];

    let _ = stream.read(&mut buffer);

    println!("Ответ от клиента: {}", String::from_utf8_lossy(&buffer));

    let response = "Привет от сервера!".as_bytes();

    let _ = stream.write(response);


    while String::from_utf8_lossy(&buffer) != "quit" || String::from_utf8_lossy(&buffer) != "\0" {
        let _ = stream.read(&mut buffer);

        println!("Ответ от клиента: {}", String::from_utf8_lossy(&buffer));

        let response = "Привет от сервера!".as_bytes();

        let _ = stream.write(response);
    }
}

fn main() {
    let listener = TcpListener::bind("127.0.0.1:65000").unwrap();
    let pool = ThreadPoolBuilder::new().num_threads(CLIENT_NUM).build().unwrap();

    println!("Сервер слушает на порту 8080...");

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        println!("Клиент подключился: {:?}", stream.peer_addr());
        // Используем многопоточность с помощью пула потоков
        pool.spawn(move || {
            handle_client(stream);
        });
    }
}
