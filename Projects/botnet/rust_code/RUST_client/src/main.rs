use std::io::prelude::*;
use std::net::TcpStream;
use tqdm::tqdm;

const DEBUG: bool = false;

fn main() {
    let mut ip_address = String::new();
    let mut port = String::new();

    println!("Введите IP адрес сервера:");
    std::io::stdin().read_line(&mut ip_address).unwrap();

    println!("Введите порт сервера:");
    std::io::stdin().read_line(&mut port).unwrap();

    let ip_address = ip_address.trim();
    let port = port.trim();
    let mut response = [0; 512];

    loop {
        match TcpStream::connect(format!("{}:{}", ip_address, port)) {
            Ok(mut stream) => {
                println!("Соединение установлено!");

                let request = "Привет серверу!".as_bytes();
                let _ = stream.write(request);

                loop {
                    let _ = stream.read(&mut response);
                    println!("Ответ от сервера: {}", String::from_utf8_lossy(&response));

                    let mut user_input = String::new();
                    println!("Введите данные для сервера\n>>");
                    std::io::stdin().read_line(&mut user_input).unwrap();

                    let request = user_input.as_bytes();
                    let _ = stream.write(request);
                }
            }
            Err(_) => {
                println!("Не удалось установить соединение. Повторная попытка через 1 минуту...");
                std::thread::sleep(std::time::Duration::from_secs(60));
            }
        }
    }
}
