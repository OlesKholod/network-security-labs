use std::env;
use std::net::TcpStream;
use std::str::FromStr;
use std::io::{self, Write};

fn main() {
    // Получение аргументов командной строки
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Usage: ./port_scanner <ip_address>");
        return;
    }

    let ip_address = &args[1];

    // Сканирование портов
    for port in 1..=65535 {
        let address = format!("{}:{}", ip_address, port);
        match TcpStream::connect_timeout(&address.parse().unwrap(), std::time::Duration::from_secs(1)) {
            Ok(_) => {
                // Порт открыт
                println!("Port {} is open", port);
            }
            Err(_) => {
                // Порт закрыт
                println!("Port {} is closed", port);
            }
        }
    }
}
