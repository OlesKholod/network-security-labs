# Burpsuite

## PortSwigger
В рамках данной лабораторной работы вам предстоит выполнить задания от разработчиков BurpSuite на PortSwigger. Для выполнения лабораторной работы необходимо выполнить минимум 50 заданий на их сайте, среди которых не менее 25 заданий уровня PRACTITIONER или EXPERT. Среди заданий должны быть задания по следующим темам:
- SQL injection
- Cross-site scripting
- Cross-site request forgery (CSRF)
- XML external entity (XXE) injection
- Server-side request forgery (SSRF)
В процессе выполнения данной лабораторной работы необходимо сформировать отчёт. Как образец можно использовать файл ["Решение burpsuite.pdf"]("./Решение burpsuite.pdf")

["Ссылка на сайт Port Swigger WebSecurity Academy"](https://portswigger.net/web-security)

## OWASP Juice Shop
В рамках данной лабораторной работы вам предстоит выполнить задания на платформе OWASP Juice Shop. Необходимо выполнить 20 заданий с сложностью три звезды и выше или 10 заданий со сложностью 4 звезды и выше. В рамках выполнения данной лабораторной работы необходимо составить отчёт о проделанной работе, включив туда ход выполнения заданий по аналогии с предыдущей лабораторной работой.
В доплнение необходимо защитить данное приложение от атак. Для этого можно использовать Web Application Firewall. Его настройка продемонстрирована в файле ["Web Application Firewall Настройка.pdf"]("./Web Application Firewall Настройка.pdf").
