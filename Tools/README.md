# Демонстрация использования утилиты

Для выполнения данной лабораторной работы вам необходимо выбрать один из инструментов в lab_info.md

## Описание утилиты

Разобрать и описать функциональные возможности выбранного инструмента. Опишите особенности и несколько основных сценариев применения.

## Задание на атаку инфраструктуру

Соберите сетевой стенд для демонстрации работы утилиты. Затем продемонстрируйте - как можно осуществить атаку с применением выбранного вами инструмента: в начале желательно провести - какие действия мог осуществить злоумышленник для разведки.

## *Установка и развёртывание IPS и HIDS

Установите и разверните систему обнаружения/предотвращения вторжений (IDS/IPS) или хостовую систему обнаружения вторжений (HIDS).

Пример выполненной лабораторной работы в sqlmap.pdf

Примеры IPS:

- AIDE
- BluVector Cortex
- Check Point Quantum IPS
- Cisco NGIPS
- Fail2Ban
- Fidelis Network
- Hillstone Networks
- Kismet
- NSFOCUS
- OpenWIPS-NG
- OSSEC
- Palo Alto Networks
- Sagan
- Samhain
- Security Onion
- Semperis
- Snort
- SolarWinds Security Event Manager (SEM) IDS/IPS
- Suricata
- Trellix (McAfee + FireEye)
- Trend Micro
- Vectra Cognito
- Zeek (AKA: Bro)
- ZScalar Cloud IPS

Примеры HIDS:

- OSSEC
- Tripwire
- Wazuh
- Samhain
- Security Onion
